namespace QaStarter;

public static class Calculator
{
	public static int Add(int a, int b)
	{
		return a + b;
	}


	public static bool IsOdd(int number)
	{
		return number % 2 == 1;
	}
}
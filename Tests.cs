using Xunit;

namespace QaStarter;

public class Tests
{
	[Fact]
	public void TestAddMethod()
	{
		Assert.Equal(12, Calculator.Add(5, 7));
		Assert.Equal(4, Calculator.Add(2, 2));
		Assert.Equal(3, Calculator.Add(3, 0));
	}

	[Fact]
	public void TestName()
	{
		Assert.True(Calculator.IsOdd(7));
		Assert.False(Calculator.IsOdd(12));
	}
}
